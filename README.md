# FP_EUV

Fourier Ptychography reconstruction for a phase defect on an Extreme Ultraviolet photomask, as presented in [Wojdyla, 2018].

## Introduction
This is a series of script used for Fourier Ptychography reconstruction using data collected using the [SHARP EUV Microscope](https://sharp.lbl.gov/) at the [Advanced Light Source](http://als.lbl.gov/), a synchrotron light source user facility located at [Lawrence Berkeley National Laboratory](https://www.lbl.gov).

## Usage
To try out Fourier ptychography reconstruction, you must clone or download the whole repertory using `git clone https://awojdyla@bitbucket.org/awojdyla/fp_euv.git` in the command line (Terminal under MacOS, or Powershell under Windows 10.)

From there, you can choose to use the python reconstruction as well as the jupyter notebook that goes along side, or the Matlab script.

### Python
You can run `fp_euv.py` to reconstruct the object. 
There is no ouptut file, make sure to create your own by exporting the `E_guess` variable after the script completion.

### Jupyter notebook
The python script is broken down into an interactive Jupyter notebook `fp_euv.ipynb`.

How to use a jupyter notebook:

* Make sure you have jupyter installed (it comes automatically with the [Anaconda](https://www.anaconda.com/distribution) distribution)

* Type `jupyter notebook` in your terminal, and open `fp_euv.ipynb` in you jupyter session.

### Matlab
The Malab script `fp_euv.m` loads the data and operates an iterative reconstruction, with some visualization to check get a better understanding of how the reconstruction works.

For postprocessing, I recommend taking a look at [MIP](https://github.com/awojdyla/mip), a Matlab toolbox I've created which has some useful processing functions.

## Data

Reconstrcuted data is located in `/defect_5um/` and the raw data for reconstruction is located in `/defect_5um/fp_data/`.
* Raw data

![picture](defect_5um/fp_data/defect_5um_sx+0.00_sy+0.00.png)

* Reconstructed data (higher resolution and quantitative phase; the hue encodes the phase.)

![picture](defect_5um/defect_5um.png)

Feel free contact me if you need additional datasets.

## References
* Zheng, G., Horstmeyer, R., & Yang, C. (2013). Wide-field, high-resolution Fourier ptychographic microscopy. Nature Photonics, 7(9), 739–745. [http://doi.org/10.1038/NPHOTON.2013.187](http://doi.org/10.1038/NPHOTON.2013.187)
* Tian, L., Li, X., Ramchandran, K., & Waller, L. (2014). Multiplexed coded illumination for Fourier Ptychography with an LED array microscope. Biomedical Optics Express, 5(7), 2376–2389. [http://doi.org/10.1364/BOE.5.002376](http://doi.org/10.1364/BOE.5.002376)
* 
Antoine Wojdyla, Markus P. Benk, Patrick P. Naulleau, Kenneth A. Goldberg, "EUV photolithography mask inspection using Fourier ptychography," SPIE 10656 106560W, 106560W (2018). [http://doi.org/10.1117/12.230786]()
* Wakonig, K., Diaz, A., Bonnin, A., Stampanoni, M., Bergamaschi, A., Ihli, J., … Menzel, A. (2019). X-ray Fourier ptychography. Science Advances, 5(February). [http://doi.org/10.1126/sciadv.aav0282](http://doi.org/10.1126/sciadv.aav0282)
