# Fourier Ptychography routines for python
import numpy as np

# frequency scale
def fs(t):
    fs = 1/(t[1]-t[0])
    Nfft = len(t)
    df = fs/Nfft
    f =  np.arange(0,fs,df) - (fs-np.mod(Nfft,2)*df)/2
    return f

# centered Fourier Transform
def ft(img):
    return np.fft.fftshift( np.fft.ifft2( np.fft.ifftshift( img ) ) )

# centered Inverse Fourier Transform
def ift(IMG):
    return np.fft.fftshift( np.fft.fft2( np.fft.ifftshift( IMG ) ) )

# shifting an array id 2D
def shift(img, x_px, y_px):
    return np.roll(np.roll(img,x_px,axis=1),y_px,axis=0)
# awojdyla@lbl.gov, May 2016
