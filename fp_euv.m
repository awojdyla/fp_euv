%% Fourier ptychography Microscopy on a phase defect
%%
% awojdyla@lbl.gov
% July 2019

%% Load the data

% change the folder to match your local file structure
folder = '/Users/awojdyla/python/fp_euv/defect_5um/fp_data/';

% read filename in the data folder
curdir = dir(folder);
% number of images
N_img = length(curdir)-2;

% pre-allocate images
img = cell(1,N_img);
% pre-allocate normalized pupil coordinates
sx = cell(1,N_img);
sy = cell(1,N_img);

for i_f = 1:N_img
    filename = curdir(i_f+2).name;
    img{i_f} = double(imread([folder,filename]));
    sx{i_f} = str2double(filename(end-16:end-12));
    sy{i_f} = str2double(filename(end-8:end-4));
end

%% system parameters

% size of teh region of interest
roi_size_px = 332;
% wavelength of acquisition
lambda_m = 13.5e-9;

% effective pixel size
dx_m = 15e-9;    
% effective field size
Dx_m = roi_size_px*dx_m;

% spatial scales
x_m = (1:roi_size_px)*dx_m;
y_m = (1:roi_size_px)*dx_m;

% angular frequency scale
fs = 1/(x_m(2)-x_m(1)); Nfft = length(x_m); df = fs/Nfft;
freq_cpm = (0:df:(fs-df)) - (fs-mod(Nfft,2)*df)/2;

% centered Fourier Transform
ft = @(signal) fftshift( ifft2( ifftshift( signal ) ) );
ift = @(SIGNAL) fftshift( fft2( ifftshift( SIGNAL ) ) );

% frequency cut-off of the lense (0.33 4xNA lens
fc_lens = (asin(.33/4)/lambda_m);
% lens pupil fiter in reciprocal space
[Fx,Fy] = meshgrid(freq_cpm);
FILTER = (Fx.^2 + Fy.^2) <= fc_lens^2;

% circular shifting
circshift2 = @(input, x_shift_px, y_shift_px)...
    circshift(circshift(input',round(x_shift_px))',round(y_shift_px));

%% Algorithm initialization

spectrum_guess = ift(sqrt(img{1}));
spectrum_guess = spectrum_guess.*FILTER;
object_guess = ft(spectrum_guess);
lens_guess = double(FILTER);

% % % threshold
% % threshold  = object_lowres>=0.0*mean(object_lowres(:));

clf
subplot(121)
imagesc(abs(spectrum_guess).^0.1)
title('pupil guess')
axis image off

subplot(122)
imagesc(abs(object_guess).^2);
title('corresponding image guess')
axis image off

%%

% hyperparameters, for regularization
alpha = 1;
beta  = 1;
for k = 1:3 % general loop
    for i = 1:N_img
        idx = i;

            S_n = object_guess;
            P_n = lens_guess;
            
            X0 = round(sx{idx}*fc_lens*Dx_m);
            Y0 = round(sy{idx}*fc_lens*Dx_m);

            phi_n = P_n.*circshift2(ft(S_n),-X0,-Y0);
            
            Phi_n = ift(phi_n);
            Phi_np = sqrt(img{idx}).*exp(1i*angle(Phi_n));
            phi_np = ft(Phi_np);
            
            S_np = ift(ft(S_n) + alpha.*...
                (conj(circshift2(P_n,X0,Y0))./max(max(abs(circshift2(P_n,X0,Y0)).^2))).*...
                (circshift2(phi_np,X0,Y0)-circshift2(phi_n,X0,Y0)));
            
            P_np = P_n + beta.*...
                (conj(circshift2(ft(S_n),-X0,-Y0))./max(max(abs(circshift2(ft(S_n),-X0,-Y0)).^2))).*...
                (phi_np-phi_n);
        
        object_guess = S_np;
        lens_guess  =  P_np.*FILTER;
        
        subplot(221)
        imagesc(abs(object_guess))
        title('reconstructed amplitude')
        axis image off
        
        subplot(222)
        imagesc(angle(object_guess))
        axis image off
        title('reconstructed phase')
        
        subplot(223)
        imagesc(abs(ft(img{idx})).^0.1)
        title('current image PSD')
        axis image off
        
        subplot(224)
        imagesc(abs(ft(S_np)).^0.1)
        title('reconstructed angular spectrum')
        axis image off
        
        drawnow
    end
end

%% Display

subplot(121)
imagesc(img{1})
title('low resolution image (data)')
axis image off

subplot(122)
imagesc(abs(object_guess).^2)
title('high resolution image (reconstruction)')
axis image off
